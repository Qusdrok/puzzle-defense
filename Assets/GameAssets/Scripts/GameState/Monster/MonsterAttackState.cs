using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GamePlay.Board;
using GameAssets.Scripts.GamePlay.Monster.Castle;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GameState.Monster
{
    public class MonsterAttackState : BaseState
    {
        private GamePlay.Soldier.Soldier _soldier;

        private float _timeAttack;

        protected override void InnerStartState()
        {
            base.InnerStartState();
            _soldier = Target.GetComponent<GamePlay.Soldier.Soldier>();
            Character.stat.ChangeCharacterState(CharacterState.Attack);
        }

        protected override void InnerUpdateState()
        {
            base.InnerUpdateState();
            _timeAttack -= Time.deltaTime;

            if (_soldier == null)
            {
                ResetSoldier();
                return;
            }

            if (!(_timeAttack <= 0f))
            {
                return;
            }

            if (_soldier != null)
            {
                ((GamePlay.Monster.Monster) Character).Attack(_soldier);
                _timeAttack = Character.stat.atkSpeed;

                if (!_soldier.stat.IsAlive)
                {
                    ResetSoldier();
                }
            }
            else
            {
                ResetSoldier();
            }
        }

        private void ResetSoldier()
        {
            _soldier = Board.Instance.NextSoldierCastle(_soldier);

            if (_soldier == null)
            {
                GameManager.GameManager.Instance.OnLose();
                return;
            }

            if (Character is MonsterCastle)
            {
                Character.ChangeState(new MonsterAttackState(), _soldier);
            }
            else
            {
                Character.ChangeState(new MonsterMoveState(), _soldier);
            }
        }
    }
}
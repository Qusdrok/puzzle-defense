using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GameState.Soldier
{
    public class SoldierMoveState : BaseState
    {
        protected override void InnerStartState()
        {
            base.InnerStartState();
            Character.stat.ChangeCharacterState(CharacterState.Move);
        }

        protected override void InnerUpdateState()
        {
            base.InnerUpdateState();

            if (Target != null)
            {
                Character.MoveLerp(Target.transform.position, Character.stat.moveSpeed / 15f);

                if (Character.Distance(Target, Character.stat.atkRange))
                {
                    Character.ChangeState(new SoldierAttackState(), Target);
                }
            }
            else
            {
                Character.transform.Translate(Character.transform.up *
                                              (Character.stat.moveSpeed * Time.fixedDeltaTime));
            }
        }
    }
}
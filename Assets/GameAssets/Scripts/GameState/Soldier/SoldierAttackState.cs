using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GamePlay.Board;
using GameAssets.Scripts.GamePlay.Soldier.Castle;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GameState.Soldier
{
    public class SoldierAttackState : BaseState
    {
        private GamePlay.Monster.Monster _monster;

        private float _timeAttack;

        protected override void InnerStartState()
        {
            base.InnerStartState();
            _monster = Target.GetComponent<GamePlay.Monster.Monster>();
            Character.stat.ChangeCharacterState(CharacterState.Attack);
        }

        protected override void InnerUpdateState()
        {
            base.InnerUpdateState();
            _timeAttack -= Time.deltaTime;

            if (_monster == null)
            {
                ResetMonster();
                return;
            }

            if (!(_timeAttack <= 0f))
            {
                return;
            }

            if (_monster != null)
            {
                ((GamePlay.Soldier.Soldier) Character).Attack(_monster);
                _timeAttack = Character.stat.atkSpeed;
                
                if (!_monster.stat.IsAlive)
                {
                    ResetMonster();
                }
            }
            else
            {
                ResetMonster();
            }
        }

        private void ResetMonster()
        {
            _monster = Board.Instance.NextMonsterCastle(_monster);

            if (_monster == null)
            {
                GameManager.GameManager.Instance.OnWin();
                return;
            }

            if (Character is SoldierCastle)
            {
                Character.ChangeState(new SoldierAttackState(), _monster);
            }
            else
            {
                Character.ChangeState(new SoldierMoveState(), _monster);
            }
        }
    }
}
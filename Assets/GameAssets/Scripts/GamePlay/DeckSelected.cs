using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GamePlay
{
    public class DeckSelected : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI txtDeckLevel;
        [SerializeField] private Image imgDeck;
        
        public Deck Deck { get; private set; }
        
        public void SetDeckSelected(Deck d)
        {
            txtDeckLevel.text = $"{d.currentLevel}";
            imgDeck.sprite = d.imgDeckIcon.sprite;
            Deck = d;
        }
    }
}
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;

namespace GameAssets.Scripts.GamePlay.Monster.Castle
{
    public partial class MonsterCastle
    {
        public override void Attack(BaseCharacter bc)
        {
            var bullet = SpawnerHelper.CreateSpawner(exitPoint.position, exitPoint, bulletPrefab)
                .GetComponent<Bullet>();
            soldier = bc as Soldier.Soldier;
            bullet.Shoot(bc, stat.atk);
        }
    }
}
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Monster.Castle
{
    public partial class MonsterCastle : Monster
    {
        [SerializeField] private LayerMask soldierLayerMask;
        [SerializeField] private Transform exitPoint;
        [SerializeField] private GameObject bulletPrefab;
    }
}
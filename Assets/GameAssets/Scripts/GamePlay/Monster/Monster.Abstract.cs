using DG.Tweening;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameInterface;

namespace GameAssets.Scripts.GamePlay.Monster
{
    public partial class Monster : IStatus
    {
        public virtual void BeAttacked(float atk)
        {
            imgHpFilled.DOFillAmount(stat.AddHp(-atk), hpLerpSpeed);

            if (stat.IsAlive)
            {
                return;
            }

            WaveConfig.WaveConfig.Instance.AddMonsterCoin(monsterCoin);
            soldier = null;
            Death();
        }

        public virtual void Attack(BaseCharacter bc)
        {
            soldier = bc as Soldier.Soldier;

            if (soldier != null)
            {
                soldier.BeAttacked(stat.atk);
            }
        }

        public virtual void Death()
        {
            var go = gameObject;
            go.IgnoreRaycast();
            SpawnerHelper.DestroySpawner(go);
        }
    }
}
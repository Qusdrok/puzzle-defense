using GameAssets.Scripts.GameBase;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GamePlay.Monster
{
    public abstract partial class Monster : BaseCharacter
    {
        [SerializeField] private Image imgHpFilled;

        [SerializeField] private int monsterCoin;
        [SerializeField] private float hpLerpSpeed;

        protected readonly RaycastHit2D[] hit2Ds = new RaycastHit2D[100];
        protected Soldier.Soldier soldier;
        private Vector3 _screenBounds;

        private int _currentLayerMask;

        private float _width;
        private float _height;
    }
}
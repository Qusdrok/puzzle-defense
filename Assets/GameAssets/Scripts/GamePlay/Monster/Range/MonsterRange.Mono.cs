using GameAssets.Scripts.GameState.Monster;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Monster.Range
{
    public partial class MonsterRange
    {
        protected override void OnEnable()
        {
            base.OnEnable();
            ChangeState(new MonsterMoveState());
        }

        protected override void InnerUpdate()
        {
            base.InnerUpdate();
            var size = Physics2D.CircleCastNonAlloc(transform.position, stat.vision, Vector2.up, hit2Ds, stat.atkRange,
                soldierLayerMask);

            if (size <= 0)
            {
                return;
            }

            foreach (var x in hit2Ds)
            {
                if (x.collider == null)
                {
                    return;
                }

                var s = x.collider.GetComponent<Soldier.Soldier>();

                if (soldier != null)
                {
                    if (!soldier.stat.IsAlive && s != null && s.stat.IsAlive || s != null && s.stat.IsAlive &&
                        s.stat.priorityLevel > soldier.stat.priorityLevel)
                    {
                        soldier = s;
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    soldier = s;
                }

                ChangeState(new MonsterMoveState(), soldier);
                return;
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(transform.position, stat.vision);
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireSphere(transform.position, stat.atkRange);
        }
    }
}
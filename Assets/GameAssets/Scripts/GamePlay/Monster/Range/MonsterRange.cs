using System.Collections.Generic;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Monster.Range
{
    public partial class MonsterRange : Monster
    {
        [SerializeField] private LayerMask soldierLayerMask;
        [SerializeField] private Transform exitPoint;
        [SerializeField] private GameObject bulletPrefab;
        [SerializeField] private bool attackAoe;

        private readonly List<Soldier.Soldier> _targets = new List<Soldier.Soldier>();

        private void CreateBullet(BaseCharacter bc)
        {
            var bullet = SpawnerHelper.CreateSpawner(exitPoint.position, exitPoint, bulletPrefab)
                .GetComponent<Bullet>();
            soldier = bc as Soldier.Soldier;
            bullet.Shoot(bc, stat.atk);
        }
    }
}
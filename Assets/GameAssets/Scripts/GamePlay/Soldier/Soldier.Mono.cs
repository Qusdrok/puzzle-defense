using DG.Tweening;
using GameAssets.Scripts.GameManager;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Soldier
{
    public partial class Soldier
    {
        protected override void Awake()
        {
            base.Awake();
            var bounds = GetComponent<SpriteRenderer>().bounds;
            var booster = DeckManager.Instance.GetDeckBooster(name);

            stat.ChangeHp(booster.Key);
            stat.atk += booster.Value;

            _currentLayerMask = gameObject.layer;
            _screenBounds =
                GameManager.GameManager.Instance.CameraSceneToGame(new Vector3(Screen.width, Screen.height, 10f));
            _width = bounds.size.x / 2f;
            _height = bounds.size.y / 2f;
        }

        protected override void InnerLateUpdate()
        {
            base.InnerLateUpdate();
            var p = transform.position;
            p.x = Mathf.Clamp(p.x, -_screenBounds.x + _width, _screenBounds.x - _width);
            p.y = Mathf.Clamp(p.y, -_screenBounds.y + _height, _screenBounds.y - _height);
        }

        protected virtual void OnEnable()
        {
            gameObject.layer = _currentLayerMask;
            imgHpFilled.DOFillAmount(stat.ResetHp(), hpLerpSpeed);
        }
    }
}
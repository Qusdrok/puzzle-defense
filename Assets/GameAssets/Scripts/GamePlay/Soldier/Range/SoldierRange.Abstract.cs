using GameAssets.Scripts.GameBase;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Soldier.Range
{
    public partial class SoldierRange
    {
        public override void Attack(BaseCharacter bc)
        {
            if (attackAoe)
            {
                var size = Physics2D.CircleCastNonAlloc(transform.position, stat.vision, Vector2.up, hit2Ds,
                    stat.atkRange, monsterLayerMask);

                if (size > 0)
                {
                    foreach (var x in hit2Ds)
                    {
                        if (x.collider == null)
                        {
                            return;
                        }

                        var s = x.collider.GetComponent<Monster.Monster>();

                        if (s != null && s.stat.IsAlive && !_targets.Contains(s))
                        {
                            _targets.Add(s);
                        }
                    }
                }
            }

            if (_targets.Count < 1)
            {
                CreateBullet(bc);
            }
            else
            {
                foreach (var x in _targets)
                {
                    CreateBullet(x);
                }
            }
        }
    }
}
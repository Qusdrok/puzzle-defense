using System.Collections.Generic;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Soldier.Range
{
    public partial class SoldierRange : Soldier
    {
        [SerializeField] private LayerMask monsterLayerMask;
        [SerializeField] private Transform exitPoint;
        [SerializeField] private GameObject bulletPrefab;
        [SerializeField] private bool attackAoe;

        private readonly List<Monster.Monster> _targets = new List<Monster.Monster>();

        private void CreateBullet(BaseCharacter bc)
        {
            var bullet = SpawnerHelper.CreateSpawner(exitPoint.position, exitPoint, bulletPrefab)
                .GetComponent<Bullet>();
            monster = bc as Monster.Monster;
            bullet.Shoot(bc, stat.atk);
        }
    }
}
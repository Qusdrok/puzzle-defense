using GameAssets.Scripts.GameState.Soldier;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Soldier.Range
{
    public partial class SoldierRange
    {
        protected override void OnEnable()
        {
            base.OnEnable();
            ChangeState(new SoldierMoveState());
        }

        protected override void InnerUpdate()
        {
            base.InnerUpdate();
            var size = Physics2D.CircleCastNonAlloc(transform.position, stat.vision, Vector2.up, hit2Ds, stat.atkRange,
                monsterLayerMask);

            if (size <= 0)
            {
                return;
            }

            foreach (var x in hit2Ds)
            {
                if (x.collider == null)
                {
                    return;
                }

                var s = x.collider.GetComponent<Monster.Monster>();

                if (monster != null)
                {
                    if (!monster.stat.IsAlive && s != null && s.stat.IsAlive || s != null && s.stat.IsAlive &&
                        s.stat.priorityLevel > monster.stat.priorityLevel)
                    {
                        monster = s;
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    monster = s;
                }

                ChangeState(new SoldierMoveState(), monster);
                return;
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(transform.position, stat.vision);
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireSphere(transform.position, stat.atkRange);
        }
    }
}
using DG.Tweening;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameInterface;

namespace GameAssets.Scripts.GamePlay.Soldier
{
    public partial class Soldier : IStatus
    {
        public virtual void BeAttacked(float atk)
        {
            imgHpFilled.DOFillAmount(stat.AddHp(-atk), hpLerpSpeed);

            if (stat.IsAlive)
            {
                return;
            } 

            monster = null;
            Death();
        }

        public virtual void Attack(BaseCharacter bc)
        {
            monster = bc as Monster.Monster;

            if (monster != null)
            {
                monster.BeAttacked(stat.atk);
            }
        }

        public virtual void Death()
        {
            var go = gameObject;
            go.IgnoreRaycast();
            SpawnerHelper.DestroySpawner(go);
        }
    }
}
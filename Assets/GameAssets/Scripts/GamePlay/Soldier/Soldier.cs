using GameAssets.Scripts.GameBase;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GamePlay.Soldier
{
    public abstract partial class Soldier : BaseCharacter
    {
        [SerializeField] private Image imgHpFilled;

        [SerializeField] private float hpLerpSpeed;

        protected readonly RaycastHit2D[] hit2Ds = new RaycastHit2D[100];
        protected Monster.Monster monster;
        private Vector3 _screenBounds;

        private int _currentLayerMask;

        private float _width;
        private float _height;
    }
}
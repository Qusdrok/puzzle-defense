using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;

namespace GameAssets.Scripts.GamePlay.Soldier.Castle
{
    public partial class SoldierCastle
    {
        public override void Attack(BaseCharacter bc)
        {
            var bullet = SpawnerHelper.CreateSpawner(exitPoint.position, exitPoint, bulletPrefab)
                .GetComponent<Bullet>();
            monster = bc as Monster.Monster;
            bullet.Shoot(bc, stat.atk);
        }
    }
}
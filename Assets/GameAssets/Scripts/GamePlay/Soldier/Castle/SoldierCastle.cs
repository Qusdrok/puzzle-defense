using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Soldier.Castle
{
    public partial class SoldierCastle : Soldier
    {
        [SerializeField] private LayerMask monsterLayerMask;
        [SerializeField] private Transform exitPoint;
        [SerializeField] private GameObject bulletPrefab;
    }
}
using GameAssets.Scripts.GameState.Soldier;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Soldier.Castle
{
    public partial class SoldierCastle
    {
        protected override void InnerUpdate()
        {
            base.InnerUpdate();
            var size = Physics2D.CircleCastNonAlloc(transform.position, stat.atkRange, Vector2.up, hit2Ds,
                stat.atkRange, monsterLayerMask);

            if (size <= 0)
            {
                return;
            }

            foreach (var x in hit2Ds)
            {
                if (x.collider == null)
                {
                    return;
                }

                var m = x.collider.GetComponent<Monster.Monster>();

                if (monster != null)
                {
                    if (!monster.stat.IsAlive && m != null && m.stat.IsAlive || m != null && m.stat.IsAlive &&
                        m.stat.priorityLevel > monster.stat.priorityLevel)
                    {
                        monster = m;
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    monster = m;
                }

                ChangeState(new SoldierAttackState(), monster);
                return;
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(transform.position, stat.atkRange);
        }
    }
}
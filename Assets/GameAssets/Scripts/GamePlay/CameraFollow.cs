﻿using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class CameraFollow : BaseSingleton<CameraFollow>
    {
        [SerializeField] private Player.Player player;
        [SerializeField] private ParticleSystem confettiFx;
        [Range(0f, 1f)] [SerializeField] private float lerpSpeed = 0.25f;

        private Vector3 _originOffset;

        private void Start()
        {
            if (player != null)
            {
                _originOffset = transform.position - player.transform.position;
            }
        }

        protected override void InnerLateUpdate()
        {
            if (player != null)
            {
                transform.MoveLerp(player.transform.position + _originOffset, lerpSpeed, false);
            }
        }

        public void EnableConfetti()
        {
            if (confettiFx == null)
            {
                return;
            }

            confettiFx.SetActive();
        }
    }
}
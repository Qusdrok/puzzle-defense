﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.GamePlay.Piece;
using GameAssets.Scripts.General;
using Sirenix.OdinInspector;
using UnityEngine;

// ReSharper disable All

namespace GameAssets.Scripts.GamePlay.Board
{
    public partial class Board : BaseSingleton<Board>
    {
        [FoldoutGroup("Initiation")] [SerializeField]
        private List<GameObject> boosters = new List<GameObject>();

        [FoldoutGroup("Initiation")] [SerializeField]
        private List<GameObject> soldierCastles = new List<GameObject>();

        [FoldoutGroup("Initiation")] [SerializeField]
        private List<GameObject> monsterCastles = new List<GameObject>();

        [FoldoutGroup("Initiation")] [SerializeField]
        private List<GameObject> pieces = new List<GameObject>();

        [FoldoutGroup("Initiation")] [SerializeField]
        private List<Transform> pieceBgs = new List<Transform>();

        [FoldoutGroup("Initiation")] [SerializeField]
        private RectTransform castleArea;

        [FoldoutGroup("Initiation")] [SerializeField]
        private Transform soldierCastle;

        [FoldoutGroup("Initiation")] [SerializeField]
        private Transform monsterCastle;

        [SerializeField] private int row;
        [SerializeField] private int column;
        [SerializeField] private float delayDrawPiece = 0.25f;

        private readonly List<Soldier.Soldier> _soldierCastles = new List<Soldier.Soldier>();
        private readonly List<Monster.Monster> _monsterCastles = new List<Monster.Monster>();

        private readonly List<Vector3> _soldierLocations = new List<Vector3>();
        private readonly List<Vector3> _monsterLocations = new List<Vector3>();

        [Button]
        private void TestBoard()
        {
            for (int i = 0; i < Row; i++)
            {
                for (int j = 0; j < Column; j++)
                {
                    Debug.Log($"{i}, {j}, {AllPieces[i, j] == null}");
                }
            }
        }

        private async void DrawBoard()
        {
            IsDrawOrAutoMatch = true;

            for (var x = 0; x < Row; x++)
            {
                for (var y = 0; y < Column; y++)
                {
                    if (AllPieces[x, y] != null)
                    {
                        continue;
                    }

                    CreatePiece(pieceBgs[x * Column + y], x, y);
                    await UniTask.Delay(TimeSpan.FromSeconds(delayDrawPiece));
                }
            }

            IsDrawOrAutoMatch = false;
        }

        private bool CheckMatch(GameObject prefab, int x, int y)
        {
            try
            {
                return y > 1 && AllPieces[x, y - 1].CompareTag(prefab.tag) &&
                       AllPieces[x, y - 2].CompareTag(prefab.tag) ||
                       x > 1 && AllPieces[x - 1, y].CompareTag(prefab.tag) &&
                       AllPieces[x - 2, y].CompareTag(prefab.tag);
            }
            catch
            {
                Debug.Log("loi check match");
                return false;
            }
        }

        private void CreatePiece(Transform bg, int x, int y)
        {
            var gos = new List<GameObject>();
            gos.AddRange(pieces);
            bg.name = $"Piece BG ({x}, {y})";

            var p = bg.position;
            var prefab = DeckManager.Instance.RandomDeck();

            while (CheckMatch(prefab, x, y))
            {
                gos.Remove(prefab);
                prefab = DeckManager.Instance.RandomDeck();
            }

            gos.Clear();
            gos.AddRange(pieces);

            var piece = SpawnerHelper.CreateSpawner(new Vector3(p.x, p.y * 2f, p.z), bg, prefab)
                .GetComponent<Piece.Piece>();
            piece.Initiation(this, bg, x, y);
            AllPieces[x, y] = piece;
        }

        private void CreateSoldier(Piece.Piece p)
        {
            if (p is PieceBooster)
            {
                return;
            }

            var soldier = SpawnerHelper.CreateSpawner(_soldierLocations[p.Column], soldierCastle, p.SoldierPrefab)
                .GetComponent<BaseCharacter>();
            GameManager.GameManager.Instance.AddBaseCharacter(soldier);
        }

        private void DestroyAutoMatch()
        {
            var hadDestroy = false;
            IsDrawOrAutoMatch = true;

            for (var x = 0; x < Row; x++)
            {
                for (var y = 0; y < Column; y++)
                {
                    var piece = AllPieces[x, y];
                    var matches = GetMatch(piece);

                    foreach (var z in matches)
                    {
                        foreach (var p in z.Key)
                        {
                            hadDestroy = true;
                            DestroyPiece(p, ReferenceEquals(p, piece) && !z.Value.Equals(BoosterType.None)
                                ? new Action(() => CreateBooster(new PieceTemp(p), z.Value))
                                : null);
                        }
                    }
                }
            }

            if (!hadDestroy)
            {
                IsDrawOrAutoMatch = false;
                return;
            }

            DOVirtual.DelayedCall(0.5f, RefillBoard);
        }

        private void DestroyEmptyPiece()
        {
            var nulls = 0;

            for (var y = 0; y < Column; y++)
            {
                for (var x = 0; x < Row; x++)
                {
                    if (AllPieces[x, y] == null)
                    {
                        nulls++;
                    }
                    else if (nulls > 0)
                    {
                        AllPieces[x, y].Initiation(this, pieceBgs[x * Column + y - Column * nulls], x - nulls, y);
                        AllPieces[x - nulls, y] = AllPieces[x, y];
                        AllPieces[x, y] = null;
                    }
                }

                nulls = 0;
            }
        }
    }
}
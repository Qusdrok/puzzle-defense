using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.GamePlay.Piece;
using GameAssets.Scripts.GameUI.UIGameStart;
using GameAssets.Scripts.General;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.GamePlay.Board
{
    public partial class Board
    {
        public Piece.Piece[,] AllPieces { get; private set; }
        public List<Transform> AllPieceBgs => pieceBgs;

        public int Row => row;
        public int Column => column;

        public bool IsDrawOrAutoMatch { get; private set; }

        public Vector3 NewMonsterLocation => _monsterLocations[Random.Range(0, _monsterLocations.Count)];

        public void InitBoard()
        {
            var mSize = monsterCastles[0].transform.localScale.y / 4f;
            var sSize = soldierCastles[0].transform.localScale.y / 4f;
            var v = new Vector3[4];

            castleArea.GetWorldCorners(v);
            var halfWidth = (Mathf.Abs(v[0].x) + Mathf.Abs(v[3].x)) / 7;

            for (var i = 0; i < Column; i++)
            {
                var drawCastle = i % 2 == 1;
                var bottom = new Vector3(v[0].x + halfWidth * (i + 0.5f),
                    v[0].y + sSize + (drawCastle ? (i - 1) % 2 == 0 ? sSize : 0f : 0f), v[0].z);
                var top = new Vector3(v[1].x + halfWidth * (i + 0.5f),
                    v[1].y - mSize + (drawCastle ? (i - 1) % 2 == 0 ? -mSize : 0f : 0f), v[1].z);

                if (drawCastle)
                {
                    var s = SpawnerHelper.CreateSpawner(bottom, soldierCastle, soldierCastles[i / 2].gameObject)
                        .GetComponent<Soldier.Soldier>();
                    _soldierCastles.Add(s);

                    var m = SpawnerHelper.CreateSpawner(top, monsterCastle, monsterCastles[i / 2].gameObject)
                        .GetComponent<Monster.Monster>();
                    _monsterCastles.Add(m);
                    GameManager.GameManager.Instance.AddBaseCharacter(s, m);
                }

                _soldierLocations.Add(bottom);
                _monsterLocations.Add(top);
            }

            UIManager.Instance.GetUI<UIGameStart>().DoBlinkTimer();
            AllPieces = new Piece.Piece[Row, Column];
            DrawBoard();
        }

        public Soldier.Soldier NextSoldierCastle(Soldier.Soldier s)
        {
            if (!s.name.Contains("Main"))
            {
                return _soldierCastles.FirstOrDefault(x => x != null && x.stat.IsAlive);
            }

            GameManager.GameManager.Instance.OnLose();
            return null;
        }

        public Monster.Monster NextMonsterCastle(Monster.Monster m)
        {
            if (!m.name.Contains("Main"))
            {
                return _monsterCastles.FirstOrDefault(x => x != null && x.stat.IsAlive);
            }

            GameManager.GameManager.Instance.OnWin();
            return null;
        }

        public void DestroyPiece(Piece.Piece p, Action onCreateBooster = null)
        {
            if (p == null || AllPieces[p.Row, p.Column].IsMatched)
            {
                return;
            }

            var sequence = DOTween.Sequence();
            var icon = p.GetComponentInChildren<Image>();

            if (onCreateBooster == null)
            {
                AllPieces[p.Row, p.Column].IsMatched = true;
            }

            sequence.Join(p.transform.DOScale(Vector3.zero, p.LerpSpeed))
                .Join(icon.DOFade(0f, p.LerpSpeed))
                .Play();

            DOVirtual.DelayedCall(0.1f, () =>
            {
                CreateSoldier(p);
                SpawnerHelper.DestroySpawner(p.gameObject);

                if (onCreateBooster == null)
                {
                    AllPieces[p.Row, p.Column] = null;
                }
                else
                {
                    onCreateBooster();
                }
            });
        }

        public void RefillBoard()
        {
            DestroyEmptyPiece();
            DrawBoard();
            DOVirtual.DelayedCall(0.3f, DestroyAutoMatch);
        }

        public Dictionary<List<Piece.Piece>, BoosterType> GetMatch(Piece.Piece p)
        {
            var result = new Dictionary<List<Piece.Piece>, BoosterType>();
            var horizontal = new List<Piece.Piece>();
            var vertical = new List<Piece.Piece>();
            var matches = new List<Piece.Piece>();
            var booster = BoosterType.None;

            void GetBooster(int count)
            {
                switch (count)
                {
                    case 4:
                        booster = BoosterType.Match4Direction;
                        break;

                    case 5:
                        booster = BoosterType.Match9Direction;
                        break;

                    case 6:
                        booster = BoosterType.MatchSameAll;
                        break;
                }
            }

            GetMatchColumn();

            if (horizontal.Count >= 2)
            {
                horizontal.Add(p);
                matches.AddRange(horizontal);
                GetMatchRow();

                if (vertical.Count >= 2)
                {
                    var up = false;
                    var down = false;

                    foreach (var x in vertical)
                    {
                        if (x.Row < p.Row)
                        {
                            up = true;
                        }
                        else
                        {
                            down = true;
                        }
                    }

                    if (up && down)
                    {
                        booster = BoosterType.MatchSameAll;
                    }
                    else if (up || down)
                    {
                        booster = BoosterType.Match9Direction;
                    }

                    matches.AddRange(vertical);
                }
                else
                {
                    GetBooster(horizontal.Count);
                }

                result.Add(matches, booster);
                return result;
            }

            void GetMatchColumn()
            {
                if (p == null)
                {
                    return;
                }

                for (var dir = 0; dir < 2; dir++)
                {
                    for (var i = 1; i < Column; i++)
                    {
                        int x;

                        if (dir == 0)
                        {
                            // Left
                            x = p.Column - i;
                        }
                        else
                        {
                            // Right
                            x = p.Column + i;
                        }

                        if (x < 0 || x >= Column)
                        {
                            break;
                        }

                        if (AllPieces[p.Row, x] != null && !AllPieces[p.Row, x].IsMatched &&
                            AllPieces[p.Row, x].CompareTag(p.tag))
                        {
                            horizontal.Add(AllPieces[p.Row, x]);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }

            horizontal.Clear();
            GetMatchRow();

            if (vertical.Count < 2)
            {
                result.Add(matches, booster);
                return result;
            }

            vertical.Add(p);
            matches.AddRange(vertical);
            GetMatchColumn();

            if (horizontal.Count >= 2)
            {
                var left = false;
                var right = false;

                foreach (var x in horizontal)
                {
                    if (x.Column < p.Column)
                    {
                        left = true;
                    }
                    else
                    {
                        right = true;
                    }
                }

                if (left && right)
                {
                    booster = BoosterType.MatchSameAll;
                }
                else if (left || right)
                {
                    booster = BoosterType.Match9Direction;
                }

                matches.AddRange(horizontal);
            }
            else
            {
                GetBooster(vertical.Count);
            }

            result.Add(matches, booster);
            return result;

            void GetMatchRow()
            {
                if (p == null)
                {
                    return;
                }

                for (var dir = 0; dir < 2; dir++)
                {
                    for (var i = 1; i < Row; i++)
                    {
                        int y;

                        if (dir == 0)
                        {
                            // Up
                            y = p.Row - i;
                        }
                        else
                        {
                            // Down
                            y = p.Row + i;
                        }

                        if (y < 0 || y >= Row)
                        {
                            break;
                        }

                        if (AllPieces[y, p.Column] != null && !AllPieces[y, p.Column].IsMatched &&
                            AllPieces[y, p.Column].CompareTag(p.tag))
                        {
                            vertical.Add(AllPieces[y, p.Column]);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
        }

        public void CreateBooster(PieceTemp tmp, BoosterType boosterType)
        {
            if (boosterType.Equals(BoosterType.None))
            {
                return;
            }

            foreach (var x in boosters)
            {
                var pb = x.GetComponent<PieceBooster>();

                if (!pb.BoosterType.Equals(boosterType))
                {
                    continue;
                }

                var piece = SpawnerHelper.CreateSpawner(tmp.parent.position, tmp.parent, x)
                    .GetComponent<Piece.Piece>();
                piece.name = $"{pb.name}";
                piece.Initiation(this, tmp.parent, tmp.row, tmp.column);
                AllPieces[tmp.row, tmp.column] = piece;
                break;
            }
        }
    }
}
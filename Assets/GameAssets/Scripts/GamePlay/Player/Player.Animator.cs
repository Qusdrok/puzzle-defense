
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Player
{
    public enum AnimationType
    {
        Attack,
        Run,
        Idle,
        Walk,
        Death
    }

    public partial class Player
    {
        [SerializeField] private Animator animator;

        public AnimationType currentAnimation;

        public void ChangeAnimation(AnimationType pa)
        {
            currentAnimation = pa;
            animator.SetTrigger(pa.ToString());
        }

        public void ChangeAnimatorLayer(string layerName, bool active = true)
        {
            for (var i = 0; i < animator.layerCount; i++)
            {
                animator.SetLayerWeight(i, 0);
            }

            animator.SetLayerWeight(animator.GetLayerIndex(layerName), active ? 1 : 0);
        }
    }
}
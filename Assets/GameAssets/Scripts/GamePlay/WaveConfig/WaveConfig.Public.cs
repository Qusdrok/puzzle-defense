namespace GameAssets.Scripts.GamePlay.WaveConfig
{
    public partial class WaveConfig
    {
        public int MonsterCoinWin { get; private set; }

        public void InitWave()
        {
            _startGame = true;

            if (spawnerAtStart)
            {
                _waveSpawnerTime = 0f;
            }
            else
            {
                WaveSpawnerTime();
            }
        }

        public void AddMonsterCoin(int coin)
        {
            MonsterCoinWin += coin;
        }
    }
}
using System.Collections.Generic;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.GamePlay.WaveConfig
{
    public partial class WaveConfig : BaseSingleton<WaveConfig>
    {
        [SerializeField] private List<GameObject> monsters = new List<GameObject>();
        [SerializeField] private Transform monsterLocations;
        [SerializeField] private Board.Board board;

        [Range(2, 7)] [SerializeField] private int maxAmount;
        [Header("Wave Time")] [SerializeField] private float waveMinTime = 2f;
        [SerializeField] private float waveMaxTime = 4f;

        [SerializeField] private int numberOfReductionWaveTime = 1;
        [SerializeField] private float slowTime = 2f;
        [SerializeField] private float reductionWaveTime = 0.5f;
        [SerializeField] private float minimumTime = 1f;
        [SerializeField] private bool spawnerAtStart;

        private int _numberSpawnerMonster;

        private float _waveSpawnerTime;
        private float _waveTime;
        
        private bool _startGame;

        private void WaveSpawnerTime()
        {
            var decTime = _numberSpawnerMonster / numberOfReductionWaveTime * reductionWaveTime;
            var maxTime = waveMaxTime - decTime < minimumTime ? minimumTime : waveMaxTime - decTime;
            _waveSpawnerTime = Random.Range(waveMinTime, maxTime);
        }

        protected override void InnerUpdate()
        {
            base.InnerUpdate();
            
            if (!_startGame)
            {
                return;
            }

            _waveTime += Time.deltaTime / slowTime;

            if (_waveTime >= _waveSpawnerTime)
            {
                SpawnerMonster();
            }
        }

        private void SpawnerMonster()
        {
            var range = Random.Range(1, maxAmount);
            var position = Vector3.zero;

            for (var i = 0; i < range; i++)
            {
                var newPosition = board.NewMonsterLocation;

                if (newPosition == position)
                {
                    continue;
                }

                var m = SpawnerHelper.CreateSpawner(newPosition, monsterLocations,
                    monsters[Random.Range(0, monsters.Count)]).GetComponent<BaseCharacter>();
                GameManager.GameManager.Instance.AddBaseCharacter(m);
            }

            _waveTime = 0f;
            _numberSpawnerMonster++;
            WaveSpawnerTime();
        }
    }
}
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private float moveSpeed;

        private BaseCharacter _target;

        private float _atk;

        private void LateUpdate()
        {
            if (_target == null)
            {
                return;
            }

            if (!_target.stat.IsAlive)
            {
                _target = null;
                Destroy();
                return;
            }

            transform.MoveLerp(_target.transform.position, moveSpeed);
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            switch (other.collider.tag)
            {
                case "Monster":
                case "FlyMonster":
                case "RangeMonster":
                case "MagicianMonster":
                case "CastleMonster":
                    var m = other.collider.GetComponent<Monster.Monster>();

                    if (m.rb != null)
                    {
                        m.rb.ResetInertia();
                    }

                    m.BeAttacked(_atk);
                    Destroy();
                    break;

                case "Soldier":
                case "FlySoldier":
                case "RangeSoldier":
                case "MagicianSoldier":
                case "CastleSoldier":
                    var s = other.collider.GetComponent<Soldier.Soldier>();

                    if (s.rb != null)
                    {
                        s.rb.ResetInertia();
                    }

                    s.BeAttacked(_atk);
                    Destroy();
                    break;
            }
        }

        public void Shoot(BaseCharacter bc, float atk)
        {
            _target = bc;
            _atk = atk;
        }

        private void Destroy()
        {
            SpawnerHelper.DestroySpawner(gameObject);
        }
    }
}
using System;
using GameAssets.Scripts.General;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GamePlay.Piece
{
    public partial class Piece : MonoBehaviour
    {
        [SerializeField] private PieceType pieceType;

        [ShowIf("@this.pieceType != GameAssets.Scripts.General.PieceType.Booster")] [SerializeField]
        private GameObject soldierPrefab;

        [SerializeField] private float lerpSpeed = 0.6f;

        protected Board.Board board;
        protected Piece otherPiece;
        private Image _icon;

        protected Vector3 originalPosition;
        protected Vector3 endPosition;

        private void Awake()
        {
            _icon = GetComponent<Image>();
        }

        protected void CalculateAngle(Action onClick = null)
        {
            var swipeAngle = Mathf.Atan2(endPosition.y - originalPosition.y, endPosition.x - originalPosition.x) *
                180f / Mathf.PI;

            if (swipeAngle == 0f)
            {
                onClick?.Invoke();
                ResetOtherPiece();
                return;
            }

            if (swipeAngle > -45f && swipeAngle <= 45f && Column < board.Column - 1)
            {
                // Right swipe
                otherPiece = board.AllPieces[Row, Column + 1];
            }
            else if (swipeAngle > 45f && swipeAngle <= 135f && Row > 0)
            {
                // Up swipe
                otherPiece = board.AllPieces[Row - 1, Column];
            }
            else if ((swipeAngle > 135f || swipeAngle <= -135f) && Column > 0)
            {
                // Left swipe
                otherPiece = board.AllPieces[Row, Column - 1];
            }
            else if (swipeAngle < -45f && swipeAngle >= -135f && Row < board.Row - 1)
            {
                // Down swipe
                otherPiece = board.AllPieces[Row + 1, Column];
            }
        }

        private void DestroyMatch(Piece p, Action onDontDestroy)
        {
            var matches = board.GetMatch(p);

            foreach (var x in matches)
            {
                if (x.Key.Count > 1)
                {
                    foreach (var z in x.Key)
                    {
                        board.DestroyPiece(z,
                            ReferenceEquals(z, p) && !x.Value.Equals(BoosterType.None)
                                ? new Action(() => board.CreateBooster(new PieceTemp(z), x.Value))
                                : null);
                    }
                }
                else
                {
                    onDontDestroy?.Invoke();
                }
            }
        }

        protected void ResetOtherPiece()
        {
            otherPiece = null;
            originalPosition = endPosition = Vector3.zero;
        }
    }
}
using System;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameAssets.Scripts.GameInterface;
using GameAssets.Scripts.General;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameAssets.Scripts.GamePlay.Piece
{
    public class PieceBooster : Piece, IPieceBooster
    {
        [SerializeField] private BoosterAction boosterAction;
        [SerializeField] private BoosterType boosterType;

        public BoosterType BoosterType => boosterType;

        private void OnEnable()
        {
            PieceType = PieceType.Booster;
        }

        public override void OnPointerDown(PointerEventData ped)
        {
            originalPosition = ped.position;
        }

        public override async void OnPointerUp(PointerEventData ped)
        {
            endPosition = ped.position;
            CalculateAngle(() =>
            {
                if (!boosterAction.Equals(BoosterAction.Touch))
                {
                    return;
                }
                
                DoBooster(() => { });
            });

            if (otherPiece == null)
            {
                ResetOtherPiece();
                return;
            }

            var tmpPosition1 = otherPiece.transform.position;
            var tmpPosition2 = transform.position;

            var tmp = new PieceTemp(otherPiece);
            var sequence = DOTween.Sequence();

            await sequence.Join(transform.DOMove(tmpPosition1, LerpSpeed))
                .Join(otherPiece.transform.DOMove(tmpPosition2, LerpSpeed)).Play()
                .AsyncWaitForCompletion();

            otherPiece.SwapPiece(new PieceTemp(this));
            SwapPiece(tmp);

            await UniTask.Delay(TimeSpan.FromSeconds(0.15f));

            async void ReturnSwap()
            {
                await sequence.Join(transform.DOMove(tmpPosition2, LerpSpeed))
                    .Join(otherPiece.transform.DOMove(tmpPosition1, LerpSpeed))
                    .Play().AsyncWaitForCompletion();

                SwapPiece(new PieceTemp(otherPiece));
                otherPiece.SwapPiece(tmp);
            }

            switch (boosterAction)
            {
                case BoosterAction.Swap:
                case BoosterAction.Both:
                    DoBooster(ReturnSwap);
                    break;
                
                case BoosterAction.Touch:
                    goto default;
                    
                default:
                    ReturnSwap();
                    break;
            }
        }

        public void DoBooster(Action onFailed)
        {
            switch (boosterType)
            {
                case BoosterType.None:
                    break;

                case BoosterType.MatchSameAll:
                    if (otherPiece != null)
                    {
                        for (var x = 0; x < board.Row; x++)
                        {
                            for (var y = 0; y < board.Column; y++)
                            {
                                var p = board.AllPieces[x, y];

                                if (p != null && !p.IsMatched && p.CompareTag(otherPiece.tag))
                                {
                                    board.DestroyPiece(p);
                                }
                            }
                        }

                        board.DestroyPiece(this);
                    }
                    else
                    {
                        onFailed.Invoke();
                    }

                    break;

                case BoosterType.Match4Direction:
                    // Up
                    if (Row > 0)
                    {
                        CheckBooster(board.AllPieces[Row - 1, Column]);
                    }

                    // Down
                    if (Row < board.Row - 1)
                    {
                        CheckBooster(board.AllPieces[Row + 1, Column]);
                    }

                    // Left
                    if (Column > 0)
                    {
                        CheckBooster(board.AllPieces[Row, Column - 1]);
                    }

                    // Right
                    if (Column < board.Column - 1)
                    {
                        CheckBooster(board.AllPieces[Row, Column + 1]);
                    }

                    board.DestroyPiece(this);
                    break;

                case BoosterType.Match9Direction:
                    // Up
                    if (Row > 0)
                    {
                        CheckBooster(board.AllPieces[Row - 1, Column]);

                        // Up Right
                        if (Column < board.Column - 1)
                        {
                            CheckBooster(board.AllPieces[Row - 1, Column + 1]);
                        }

                        // Up Left
                        if (Column > 0)
                        {
                            CheckBooster(board.AllPieces[Row - 1, Column - 1]);
                        }
                    }

                    // Down
                    if (Row < board.Row - 1)
                    {
                        CheckBooster(board.AllPieces[Row + 1, Column]);

                        // Down Right
                        if (Column < board.Column - 1)
                        {
                            CheckBooster(board.AllPieces[Row + 1, Column + 1]);
                        }

                        // Down Left
                        if (Column > 0)
                        {
                            CheckBooster(board.AllPieces[Row + 1, Column - 1]);
                        }
                    }

                    // Left
                    if (Column > 0)
                    {
                        CheckBooster(board.AllPieces[Row, Column - 1]);
                    }

                    // Right
                    if (Column < board.Column - 1)
                    {
                        CheckBooster(board.AllPieces[Row, Column + 1]);
                    }

                    board.DestroyPiece(this);
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            DOVirtual.DelayedCall(1f, board.RefillBoard);
        }

        private void CheckBooster(Piece p)
        {
            if (p == null)
            {
                return;
            }

            if (p.PieceType.Equals(PieceType.Booster))
            {
                p.PieceType = PieceType.Normal;
                (p as PieceBooster)?.DoBooster(() => { });
            }
            else
            {
                board.DestroyPiece(p);
            }
        }
    }
}
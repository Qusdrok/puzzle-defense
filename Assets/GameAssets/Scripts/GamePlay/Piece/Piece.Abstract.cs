using System;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameAssets.Scripts.GamePlay.Piece
{
    public readonly struct PieceTemp
    {
        public readonly Transform parent;
        public readonly int row;
        public readonly int column;

        public PieceTemp(Piece p)
        {
            row = p.Row;
            column = p.Column;
            parent = p.Parent;
        }
    }

    public partial class Piece : IPointerDownHandler, IPointerUpHandler
    {
        public virtual void OnPointerDown(PointerEventData ped)
        {
            originalPosition = ped.position;
        }

        public virtual async void OnPointerUp(PointerEventData ped)
        {
            endPosition = ped.position;
            CalculateAngle();

            if (otherPiece == null || otherPiece.IsMatched || board.IsDrawOrAutoMatch ||
                !GameManager.GameManager.Instance.IsGameState(General.GameState.Playing))
            {
                ResetOtherPiece();
                return;
            }

            var tmpPosition1 = otherPiece.transform.position;
            var tmpPosition2 = transform.position;

            var tmp = new PieceTemp(otherPiece);
            var sequence = DOTween.Sequence();

            await sequence.Join(transform.DOMove(tmpPosition1, LerpSpeed))
                .Join(otherPiece.transform.DOMove(tmpPosition2, LerpSpeed))
                .Play().AsyncWaitForCompletion();

            otherPiece.SwapPiece(new PieceTemp(this));
            SwapPiece(tmp);

            await UniTask.Delay(TimeSpan.FromSeconds(0.15f));

            async void ReturnSwap()
            {
                if (!sequence.IsActive())
                {
                    sequence = DOTween.Sequence();
                }

                await sequence.Join(transform.DOMove(tmpPosition2, LerpSpeed))
                    .Join(otherPiece.transform.DOMove(tmpPosition1, LerpSpeed))
                    .Play().AsyncWaitForCompletion();

                SwapPiece(new PieceTemp(otherPiece));
                otherPiece.SwapPiece(tmp);
            }

            DestroyMatch(this, () => DestroyMatch(otherPiece, ReturnSwap));
            DestroyMatch(otherPiece, null);

            DOVirtual.DelayedCall(LerpSpeed * 2f, ()=>
            {
                board.RefillBoard();
                ResetOtherPiece();
            });
        }
    }
}
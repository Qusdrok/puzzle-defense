using DG.Tweening;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Piece
{
    public partial class Piece
    {
        public GameObject SoldierPrefab => soldierPrefab;

        public PieceType PieceType
        {
            get => pieceType;
            set => pieceType = value;
        }

        public Transform Parent { get; private set; }

        public int Row { get; private set; }
        public int Column { get; private set; }

        public float LerpSpeed => lerpSpeed;

        public bool IsMatched { get; set; }

        public void Initiation(Board.Board b, Transform parent, int x, int y)
        {
            Row = x;
            Column = y;

            board = b;
            Parent = parent;
            IsMatched = false;

            var sequence = DOTween.Sequence();
            sequence.Join(_icon.DOFade(1f, 0f))
                .Join(transform.DOScale(Vector3.one, 0f))
                .Join(transform.DOMove(Parent.position, lerpSpeed))
                .Play().OnComplete(() =>
                {
                    var t = transform;
                    t.SetParent(Parent);
                    t.position = Parent.position;
                });
        }

        public void SwapPiece(PieceTemp p)
        {
            Row = p.row;
            Column = p.column;
            Parent = p.parent;

            transform.SetParent(Parent);
            board.AllPieces[Row, Column] = this;
        }
    }
}
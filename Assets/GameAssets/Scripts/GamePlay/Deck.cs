using System;
using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.GameUI.UIPlay;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GamePlay
{
    public class Deck : MonoBehaviour
    {
        public List<float> priceToLevels = new List<float>();
        public GameObject piecePrefab;
        public Image imgDeckIcon;

        [Header("UI Variables")] public TextMeshProUGUI txtLevel;
        [HideIf(nameof(unlocked))] public TextMeshProUGUI txtPriceUnlock;
        [HideIf(nameof(unlocked))] public Image imgUnlock;

        public TextMeshProUGUI txtPrice;
        public Button btnUpgrade;
        public Button btnSelect;
        public Image imgSelect;

        [Header("Config")] [HideIf(nameof(unlocked))]
        public int priceUnlock = 20;

        public int currentLevel = 1;
        public int maxLevel = 20;
        public float hpByLevel = 1;
        public float atkByLevel = 1;
        public bool unlocked;

        private UIPlay _uiPlay;

        private void Awake()
        {
            _uiPlay = UIManager.Instance.GetUI<UIPlay>();

            if (unlocked)
            {
                DisableUnlock();
                return;
            }

            currentLevel = 0;
            btnUpgrade.gameObject.SetActive(false);
            imgUnlock.gameObject.SetActive(true);
            txtPriceUnlock.gameObject.SetActive(true);
            txtPriceUnlock.text = $"{priceUnlock}";
        }

        private void DisableUnlock()
        {
            if (txtPriceUnlock == null || imgUnlock == null)
            {
                return;
            }

            btnUpgrade.gameObject.SetActive(true);
            txtPriceUnlock.gameObject.SetActive(false);
            imgUnlock.gameObject.SetActive(false);
        }

        private void LevelUp()
        {
            if (currentLevel >= maxLevel || !UserDataManager.Instance.CheckAndChangeMoney(GetPrice))
            {
                return;
            }

            currentLevel++;
            SetText();
            _uiPlay.SetDeck();
        }

        public void UpdateDeckState()
        {
            if (unlocked)
            {
                DisableUnlock();
            }

            SetText();
            btnUpgrade.interactable =
                UserDataManager.Instance.userDataSave.money >= GetPrice && currentLevel < maxLevel;
        }

        public void AddListener(Action onEvent)
        {
            btnUpgrade.onClick.AddListener(() =>
            {
                LevelUp();
                onEvent.Invoke();
            });

            btnSelect.onClick.AddListener(() =>
            {
                if (unlocked)
                {
                    DeckManager.Instance.AddDeck(this);
                }
                else
                {
                    var canUnlock = UserDataManager.Instance.CheckAndChangeMoney(priceUnlock);

                    if (canUnlock)
                    {
                        unlocked = true;
                        currentLevel++;
                        btnUpgrade.gameObject.SetActive(true);
                        _uiPlay.SetDeck();
                        onEvent.Invoke();
                    }
                    else
                    {
                        SpawnerHelper.CreateToast("You dont enough money to unlock it");
                    }
                }
            });

            onEvent.Invoke();
        }

        public float GetHp => currentLevel * hpByLevel;
        public float GetAtk => currentLevel * atkByLevel;

        public void SetSelect(bool active)
        {
            imgSelect.gameObject.SetActive(active);
        }

        public void SetDeck(int level)
        {
            currentLevel = level;

            if (!unlocked)
            {
                unlocked = true;
                DisableUnlock();
            }

            SetText();
        }

        private void SetText()
        {
            txtLevel.text = $"Level {currentLevel}";

            if (currentLevel >= maxLevel)
            {
                txtPrice.text = "MAX";
                btnUpgrade.interactable = false;
            }
            else
            {
                txtPrice.text = $"{GetPrice}";
            }
        }

        private float GetPrice => priceToLevels.Where((t, i) => currentLevel > i).Sum();
    }
}
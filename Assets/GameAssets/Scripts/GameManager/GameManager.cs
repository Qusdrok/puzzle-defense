﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameModel;
using GameAssets.Scripts.GamePlay.Monster;
using GameAssets.Scripts.GamePlay.Soldier;
using GameAssets.Scripts.GameUI.UIGameComplete;
using GameAssets.Scripts.GameUI.UIGameDraw;
using GameAssets.Scripts.GameUI.UIGameOver;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GameManager
{
    [RequireComponent(typeof(SpawnerHelper))]
    public class GameManager : BaseSingleton<GameManager>
    {
        [SerializeField] private List<BaseCharacter> characters = new List<BaseCharacter>();
        [SerializeField] private new Camera camera;

        public General.GameState currentState;

        protected override void Awake()
        {
            base.Awake();
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 120;
            AddBaseCharacter(FindObjectsOfType<BaseCharacter>());
        }

        public void AddBaseCharacter(params BaseCharacter[] character)
        {
            foreach (var x in character)
            {
                if (!characters.Contains(x))
                {
                    characters.Add(x);
                }
            }
        }

        protected override void InnerUpdate()
        {
            base.InnerUpdate();

            foreach (var x in characters)
            {
                x.currentState?.UpdateState();
            }
        }

        public void SetGameState(General.GameState gameState)
        {
            if (Equals(currentState, gameState))
            {
                return;
            }

            currentState = gameState;
            Debug.Log($"GAME STATE ===> {gameState}");
        }

        public bool IsGameState(General.GameState state)
        {
            return currentState == state;
        }

        public void OnWin()
        {
            var uiGameComplete = UIManager.Instance.GetUI<UIGameComplete>();
            uiGameComplete.gameObject.SetActive(true);
            uiGameComplete.SetWinCoin();

            SetGameState(General.GameState.Win);
            StopBaseCharacter();
        }

        public void OnDraw()
        {
            var uiGameDraw = UIManager.Instance.GetUI<UIGameDraw>();
            uiGameDraw.gameObject.SetActive(true);

            SetGameState(General.GameState.Draw);
            StopBaseCharacter();
        }

        public void OnLose()
        {
            var uiGameOver = UIManager.Instance.GetUI<UIGameOver>();
            uiGameOver.gameObject.SetActive(true);

            SetGameState(General.GameState.Lose);
            StopBaseCharacter();
        }

        public void OnBooster(Round r)
        {
            foreach (var x in characters)
            {
                switch (r.boosterAffect)
                {
                    case BoosterAffect.All:
                        x.stat.Booster(r);
                        break;

                    case BoosterAffect.Soldier:
                        if (x is Soldier)
                        {
                            x.stat.Booster(r);
                        }

                        break;

                    case BoosterAffect.Monster:
                        if (x is Monster)
                        {
                            x.stat.Booster(r);
                        }

                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public Vector3 CameraSceneToGame(Vector3 position)
        {
            return camera.ScreenToWorldPoint(position);
        }

        public Ray CameraSceneToRay(Vector3 position)
        {
            return camera.ScreenPointToRay(position);
        }

        private void StopBaseCharacter()
        {
            foreach (var x in characters.Where(x => x != null && x.stat.IsAlive))
            {
                x.ChangeState();

                if (x.rb != null)
                {
                    x.rb.ResetInertia();
                }
            }
        }
    }
}
﻿using System.Collections.Generic;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using UnityEngine;

namespace GameAssets.Scripts.GameManager
{
    public class LevelManager : BaseSingleton<LevelManager>
    {
        [SerializeField] private List<GameObject> levels = new List<GameObject>();
        [SerializeField] private Transform parent;
        [SerializeField] private bool isLoopLevel;

        public int currentLevel;

        protected override void Awake()
        {
            base.Awake();
            currentLevel = UserDataManager.Instance.userDataSave.level;
            LoadLevel();
        }

        private void LoadLevel()
        {
            if (levels.Count <= 0)
            {
                return;
            }

            var index = isLoopLevel ? currentLevel % levels.Count :
                currentLevel > levels.Count - 1 ? levels.Count - 1 : currentLevel;
            SpawnerHelper.CreateSpawner(Vector3.zero, parent, levels[index]);
        }
    }
}
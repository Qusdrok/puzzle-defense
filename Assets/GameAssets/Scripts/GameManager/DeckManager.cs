using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GamePlay;
using UnityEngine;

namespace GameAssets.Scripts.GameManager
{
    public class DeckManager : BaseSingleton<DeckManager>
    {
        [SerializeField] private GameObject deckSelectedPrefab;
        [SerializeField] private int select = 5;

        public int Select => select;

        private readonly List<DeckSelected> _deckSelecteds = new List<DeckSelected>();
        private readonly List<Deck> _deckSelects = new List<Deck>();

        public void AddDeck(Deck d)
        {
            if (_deckSelects.Contains(d))
            {
                d.SetSelect(false);
                _deckSelects.Remove(d);
                return;
            }

            d.SetSelect(true);
            _deckSelects.Add(d);
        }

        public GameObject RandomDeck()
        {
            var drop = _deckSelects.Select(x => x.currentLevel).Select(y => (float) y).ToList().DropChance();
            return (from x in _deckSelects where x.currentLevel.Equals((int) drop) select x.piecePrefab)
                .FirstOrDefault();
        }

        public void DrawDeckSelected(Transform deckSelectedArea)
        {
            foreach (var x in _deckSelects)
            {
                var ds = Instantiate(deckSelectedPrefab, deckSelectedArea).GetComponent<DeckSelected>();
                _deckSelecteds.Add(ds);
                ds.SetDeckSelected(x);
            }
        }

        public KeyValuePair<float, float> GetDeckBooster(string name)
        {
            foreach (var x in from x in _deckSelecteds
                let deck = x.Deck.name.Match(' ', '(')
                where deck.Equals(name.Match(' ', '('))
                select x)
            {
                return new KeyValuePair<float, float>(x.Deck.GetHp, x.Deck.GetAtk);
            }

            return default;
        }

        public bool CheckStart => _deckSelects.Count == select;
    }
}
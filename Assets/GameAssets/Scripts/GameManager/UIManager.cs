﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameUI;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GameManager
{
    public class UIManager : BaseSingleton<UIManager>
    {
        [SerializeField] private List<GameObject> uis = new List<GameObject>();
        [SerializeField] private UIType uiAwake;

        private UIDialog _uiDialog;

        protected override void Awake()
        {
            base.Awake();
            HideAllAndShowUI(uiAwake);
            UIHelper.NetworkEvent(() => EnableDialog("Error network", _ => Application.Quit()));
        }

        public void HideAllAndShowUI(UIType type)
        {
            foreach (var ui in uis.Select(x => x.GetComponent<BaseUI>()))
            {
                ui.DoAnimation(ui.UIType.Equals(type));
            }
        }

        public T GetUI<T>() where T : BaseUI
        {
            return uis.Select(x => x.GetComponent<T>()).FirstOrDefault(t => t != null);
        }

        public void EnableDialog(string title, Action<GameObject> onAction)
        {
            if (_uiDialog == null)
            {
                _uiDialog = GetUI<UIDialog>();
            }

            _uiDialog.SetDialog(title, () => onAction.Invoke(_uiDialog.gameObject));
        }
    }
}
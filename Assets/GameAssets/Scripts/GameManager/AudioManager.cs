﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameModel;
using UnityEngine;

namespace GameAssets.Scripts.GameManager
{
    public class AudioManager : BaseSingleton<AudioManager>
    {
        [SerializeField] private List<AudioConfig> audioClips = new List<AudioConfig>();

        [Header("Audio Asset")] [SerializeField]
        private AudioAsset audioAsset;

        [SerializeField] private AudioSource audioSound;
        [SerializeField] private AudioSource audioMusic;

        private int _loop;

        protected override void Awake()
        {
            base.Awake();
            OnEnableAudio();
        }

        private void OnEnableAudio()
        {
            OnUpdateSound();
            OnUpdateMusic();

            if (audioMusic.volume >= 1)
            {
                PlayMusic("Background");
            }
            else
            {
                StopMusic();
            }
        }

        public void OnUpdateSound()
        {
            audioSound.volume = UserDataManager.Instance.userDataSave.sound ? 1 : 0;
        }

        public void OnUpdateMusic()
        {
            audioMusic.volume = UserDataManager.Instance.userDataSave.music ? 1 : 0;
        }

        private void PlayMusic(string clipName)
        {
            var clip = GetClip(clipName);

            if (clip == null)
            {
                return;
            }

            audioMusic.clip = clip;
            audioMusic.Play();
        }

        public void StopMusic()
        {
            if (audioMusic.isPlaying)
            {
                audioMusic.Stop();
            }
        }

        public void PlaySoundLoop(string clipName, int loopTimes = 1)
        {
            var clip = GetClip(clipName);

            if (loopTimes < 2)
            {
                audioSound.PlayOneShot(clip);
            }
            else
            {
                _loop = 0;
                audioSound.clip = clip;
                StartCoroutine(Loop(clip, loopTimes));
            }
        }

        public void StopSound()
        {
            if (audioSound.isPlaying)
            {
                audioSound.Stop();
            }
        }

        public void PlayVibrate()
        {
            if (UserDataManager.Instance.userDataSave.vibration)
            {
                AndroidVibration.Vibrate();
            }
        }

        private AudioClip GetClip(string soundName)
        {
            return (from x in audioClips where x.clipName.Equals(soundName) select x.clipSound).FirstOrDefault();
        }

        private IEnumerator Loop(AudioClip clip, int loop)
        {
            audioSound.Play();
            _loop += 1;

            yield return new WaitForSeconds(clip.length / 2);

            if (_loop < loop)
            {
                StartCoroutine(Loop(clip, loop));
            }
        }
    }
}
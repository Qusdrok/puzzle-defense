﻿using UnityEngine;

namespace GameAssets.Scripts.GameBase.Interface
{
    public interface IDestruction
    {
        void Destroy(Vector3 point, float force);
    }
}
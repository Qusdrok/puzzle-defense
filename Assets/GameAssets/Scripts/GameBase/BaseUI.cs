﻿using DG.Tweening;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GameBase
{
    public class BaseUI : MonoBehaviour
    {
        [SerializeField] private AnimationType animationType;
        [SerializeField] private Ease ease;
        [SerializeField] private UIType uiType;

        [SerializeField] private float durationOn = 0.2f;
        [SerializeField] private float durationOff = 0.02f;

        public UIType UIType => uiType;

        private RectTransform _rt;

        protected virtual void Awake()
        {
            _rt = GetComponent<RectTransform>();
        }

        public void DoAnimation(bool active)
        {
            if (active)
            {
                if (gameObject.activeInHierarchy)
                {
                    return;
                }

                gameObject.SetActive(true);
            }
            else
            {
                if (!gameObject.activeInHierarchy)
                {
                    return;
                }

                gameObject.SetActive(false);
            }
        }
    }
}
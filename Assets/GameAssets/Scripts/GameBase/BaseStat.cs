using GameAssets.Scripts.GameModel;
using GameAssets.Scripts.General;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GameBase
{
    public sealed class BaseStat : MonoBehaviour
    {
        public CharacterState characterState;
        public PriorityLevel priorityLevel;
        public UnitType unitType;

        public float hp;
        public float atk;
        public float atkSpeed;
        public float atkRange;

        [ShowIf("@this.unitType != GameAssets.Scripts.General.UnitType.Castle")]
        public float vision;

        [ShowIf("@this.unitType != GameAssets.Scripts.General.UnitType.Castle")]
        public float moveSpeed;

        private bool IsMove { get; set; }
        public bool IsAttack { get; set; }
        public bool IsAlive { get; private set; } = true;

        private float _hp;

        public float ResetHp()
        {
            _hp = hp;
            ChangeCharacterState(CharacterState.IdleOrRespawn);
            return _hp;
        }

        public void Booster(Round r)
        {
            AddHp(_hp + r.hp / 100);
            _hp += _hp + r.hp / 100;
            atk += atk + r.atk / 100;
            atkSpeed += atkSpeed + r.atkSpeed / 100;
            moveSpeed += moveSpeed + r.moveSpeed / 100;
        }

        public float AddHp(float h)
        {
            _hp += h;

            if (!(_hp <= 0))
            {
                return _hp / hp;
            }

            ChangeCharacterState(CharacterState.Death);
            return 0f;
        }

        public void ChangeHp(float h)
        {
            hp += h;
            _hp = hp;
        }

        public bool IsCharacterState(CharacterState state)
        {
            return characterState.Equals(state);
        }

        public void SetCharacterState(CharacterState state)
        {
            characterState = state;
        }

        public bool ChangeCharacterState(CharacterState state)
        {
            if (IsCharacterState(state))
            {
                return false;
            }

            SetCharacterState(state);
            /*Debug.Log($"{name} CS ===> {state}");*/

            switch (state)
            {
                case CharacterState.IdleOrRespawn:
                    IsMove = false;
                    IsAlive = true;
                    IsAttack = false;
                    break;

                case CharacterState.Move:
                    IsMove = true;
                    IsAttack = false;
                    IsAlive = true;
                    break;

                case CharacterState.Attack:
                    IsMove = false;
                    IsAttack = true;
                    IsAlive = true;
                    break;

                case CharacterState.Death:
                    IsMove = false;
                    IsAttack = false;
                    IsAlive = false;
                    break;
            }

            return true;
        }
    }
}
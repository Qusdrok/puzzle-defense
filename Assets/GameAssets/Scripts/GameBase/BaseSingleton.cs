﻿using UnityEngine;

namespace GameAssets.Scripts.GameBase
{
    public class BaseSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        [SerializeField] private bool isDontDestroy;
        
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<T>();
                }

                return _instance;
            }
            private set => _instance = value;
        }

        protected virtual void Awake()
        {
            if (Instance == null || !isDontDestroy)
            {
                return;
            }

            if (transform.parent != null)
            {
                transform.SetParent(null);
            }

            DontDestroyOnLoad(this);
        }

        private void Update()
        {
            if (!GameManager.GameManager.Instance.IsGameState(General.GameState.Playing))
            {
                return;
            }

            InnerUpdate();
        }

        private void FixedUpdate()
        {
            if (!GameManager.GameManager.Instance.IsGameState(General.GameState.Playing))
            {
                return;
            }

            InnerFixedUpdate();
        }

        private void LateUpdate()
        {
            if (!GameManager.GameManager.Instance.IsGameState(General.GameState.Playing))
            {
                return;
            }

            InnerFixedUpdate();
        }

        protected virtual void InnerUpdate()
        {
        }

        protected virtual void InnerFixedUpdate()
        {
        }

        protected virtual void InnerLateUpdate()
        {
        }

        protected virtual void OnDestroy()
        {
            if (Equals(Instance, this))
            {
                Instance = default;
            }
        }
    }
}
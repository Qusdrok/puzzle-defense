﻿using GameAssets.Scripts.GameBase.Interface;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.General;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GameBase
{
    [RequireComponent(typeof(BaseStat))]
    public abstract class BaseCharacter : MonoBehaviour
    {
        [ReadOnly] public new Collider2D collider;
        [ReadOnly] public IState currentState;

        [ReadOnly] public Rigidbody2D rb;
        [ReadOnly] public BaseStat stat;
        [ReadOnly] public string stateName;

        protected virtual void Awake()
        {
            collider = GetComponent<Collider2D>();
            rb = GetComponent<Rigidbody2D>();
            stat = GetComponent<BaseStat>();
        }

        private void Update()
        {
            if (!GameManager.GameManager.Instance.IsGameState(General.GameState.Playing) || !stat.IsAlive)
            {
                return;
            }

            InnerUpdate();
        }

        private void FixedUpdate()
        {
            if (!GameManager.GameManager.Instance.IsGameState(General.GameState.Playing) || !stat.IsAlive)
            {
                return;
            }

            InnerFixedUpdate();
        }

        private void LateUpdate()
        {
            if (!GameManager.GameManager.Instance.IsGameState(General.GameState.Playing) || !stat.IsAlive)
            {
                return;
            }

            InnerLateUpdate();
        }

        protected virtual void InnerUpdate()
        {
        }

        protected virtual void InnerFixedUpdate()
        {
        }

        protected virtual void InnerLateUpdate()
        {
        }

        public void ChangeState(IState state = null, MonoBehaviour target = null)
        {
            stateName = $"{state}";
            currentState = state;
            currentState?.ExitState();

            if (rb != null)
            {
                rb.ResetInertia();
            }

            if (state == null)
            {
                stat.ChangeCharacterState(CharacterState.IdleOrRespawn);
                return;
            }

            /*Debug.Log($"{name} STATE ===> {newState}");*/
            currentState.Character = this;
            currentState.StartState(target);
        }
    }
}
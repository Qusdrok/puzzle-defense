using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.GamePlay;
using GameAssets.Scripts.General;

namespace GameAssets.Scripts.GameUI.UIPlay
{
    public partial class UIPlay
    {
        protected override void Awake()
        {
            base.Awake();
            SetText();

            btnGameStart.onClick.AddListener(() =>
            {
                if (DeckManager.Instance.CheckStart)
                {
                    UIManager.Instance.HideAllAndShowUI(UIType.UIGameStart);
                }
                else
                {
                    SpawnerHelper.CreateToast($"You need choose {DeckManager.Instance.Select} decks");
                }
            });

            var deckSaved = UserDataManager.Instance.userDataSave.decks;

            for (var i = 0; i < decks.Count; i++)
            {
                var deck = Instantiate(decks[i], deckArea).GetComponent<Deck>();

                if (deckSaved.Count > i)
                {
                    deck.SetDeck(deckSaved[i]);
                }

                deck.AddListener(CheckAllUpgrade);
            }

            if (deckSaved.Count < 1)
            {
                SetDeck();
            }
        }
    }
}
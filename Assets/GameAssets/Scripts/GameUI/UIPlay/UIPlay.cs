using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.GamePlay;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameUI.UIPlay
{
    public partial class UIPlay : BaseUI
    {
        [SerializeField] private List<GameObject> decks = new List<GameObject>();
        [SerializeField] private TextMeshProUGUI txtCurrentMoney;
        [SerializeField] private Button btnGameStart;
        [SerializeField] private Transform deckArea;

        private void CheckAllUpgrade()
        {
            foreach (var deck in deckArea.GetComponentsInChildren<Deck>())
            {
                deck.UpdateDeckState();
            }

            SetText();
        }

        private void SetText()
        {
            txtCurrentMoney.text = $"{UserDataManager.Instance.userDataSave.money}";
        }

        public void SetDeck()
        {
            var levels = deckArea.GetComponentsInChildren<Deck>().Select(x => x.currentLevel).ToArray();
            UserDataManager.Instance.SetDecks(levels);
        }
    }
}
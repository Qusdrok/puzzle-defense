using GameAssets.Scripts.GameManager;

namespace GameAssets.Scripts.GameUI.UIGameDraw
{
    public partial class UIGameDraw
    {
        protected override void Awake()
        {
            base.Awake();
            btnGameDraw.onClick.AddListener(SceneManager.ResetCurrentScene);
        }
    }
}
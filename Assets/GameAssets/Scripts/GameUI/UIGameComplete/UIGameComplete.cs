using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.GamePlay.WaveConfig;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameUI.UIGameComplete
{
    public partial class UIGameComplete : BaseUI
    {
        [SerializeField] private Button btnGameComplete;
        [SerializeField] private TextMeshProUGUI txtWinCoin;

        public void SetWinCoin()
        {
            var money = WaveConfig.Instance.MonsterCoinWin;
            txtWinCoin.text = $"{money} Coin";
            UserDataManager.Instance.AddMoney(money);
        }
    }
}
using GameAssets.Scripts.GameManager;

namespace GameAssets.Scripts.GameUI.UIGameComplete
{
    public partial class UIGameComplete
    {
        protected override void Awake()
        {
            base.Awake();
            btnGameComplete.onClick.AddListener(SceneManager.ResetCurrentScene);
        }
    }
}
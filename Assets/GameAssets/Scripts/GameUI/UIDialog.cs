using System;
using GameAssets.Scripts.GameBase;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameUI
{
    public class UIDialog : BaseUI
    {
        [SerializeField] private TextMeshProUGUI txtError;
        [SerializeField] private Button btnError;

        public void SetDialog(string error, Action onError)
        {
            txtError.text = error;
            btnError.onClick.RemoveAllListeners();
            btnError.onClick.AddListener(onError.Invoke);
        }
    }
}
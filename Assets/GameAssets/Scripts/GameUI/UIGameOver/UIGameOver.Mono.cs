using SceneManager = GameAssets.Scripts.GameManager.SceneManager;

namespace GameAssets.Scripts.GameUI.UIGameOver
{
    public partial class UIGameOver
    {
        protected override void Awake()
        {
            base.Awake();
            btnGameOver.onClick.AddListener(SceneManager.ResetCurrentScene);
        }
    }
}
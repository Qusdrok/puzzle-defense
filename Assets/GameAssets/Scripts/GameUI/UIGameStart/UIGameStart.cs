using System;
using System.Collections.Generic;
using DG.Tweening;
using GameAssets.Scripts.GameBase;
using GameAssets.Scripts.GameModel;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.GameUI.UIGameStart
{
    public partial class UIGameStart : BaseUI
    {
        [SerializeField] private List<Round> rounds = new List<Round>();
        [SerializeField] private TextMeshProUGUI txtRoundTimer;
        [SerializeField] private TextMeshProUGUI txtRound;
        [SerializeField] private TextMeshProUGUI txtCountdown;
        [SerializeField] private Transform deckSelectedArea;
        [SerializeField] private Vector3 baseScale;

        [SerializeField] private int countdownTimer = 3;
        [SerializeField] private float maxSecond = 59f;
        [SerializeField] private float scaleSpeed = 0.25f;

        private DateTime _timeExitApp;
        
        private int _countdownTimer;
        private int _roundIndex;

        private float _roundTimer;
        private float _second;

        private bool _isStartTimer;

        public void DoBlinkTimer()
        {
            var loop = 0;
            txtRound.text = $"{rounds[_roundIndex].title}";
            txtCountdown.transform.localScale = baseScale;

            txtCountdown.gameObject.SetActive(true);
            txtRound.gameObject.SetActive(true);
            txtRoundTimer.gameObject.SetActive(false);

            txtCountdown.transform.DOScale(Vector3.one, scaleSpeed).SetEase(Ease.Linear)
                .SetLoops(countdownTimer * 2 + 1, LoopType.Yoyo)
                .OnStepComplete(() =>
                {
                    loop++;

                    if (loop % 2 != 0)
                    {
                        return;
                    }

                    _countdownTimer--;
                    txtCountdown.text = $"{_countdownTimer}";
                })
                .OnComplete(() =>
                {
                    _isStartTimer = true;
                    _roundTimer = rounds[_roundIndex].timer;
                    _countdownTimer = countdownTimer;
                    txtCountdown.text = $"{_countdownTimer}";

                    txtCountdown.gameObject.SetActive(false);
                    txtRound.gameObject.SetActive(false);
                    txtRoundTimer.gameObject.SetActive(true);
                    GameManager.GameManager.Instance.SetGameState(General.GameState.Playing);
                });
        }
    }
}
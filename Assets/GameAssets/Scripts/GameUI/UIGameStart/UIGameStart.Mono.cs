using System;
using GameAssets.Scripts.GameHelper;
using GameAssets.Scripts.GameManager;
using GameAssets.Scripts.GamePlay.Board;
using GameAssets.Scripts.GamePlay.WaveConfig;
using UnityEngine;

namespace GameAssets.Scripts.GameUI.UIGameStart
{
    public partial class UIGameStart
    {
        protected override void Awake()
        {
            base.Awake();
            Board.Instance.InitBoard();
            WaveConfig.Instance.InitWave();
            DeckManager.Instance.DrawDeckSelected(deckSelectedArea);
            _countdownTimer = countdownTimer;
        }

        private void Update()
        {
            if (!_isStartTimer || GameManager.GameManager.Instance.IsGameState(General.GameState.Win) ||
                GameManager.GameManager.Instance.IsGameState(General.GameState.Lose) ||
                GameManager.GameManager.Instance.IsGameState(General.GameState.Draw) ||
                GameManager.GameManager.Instance.IsGameState(General.GameState.Pause))
            {
                return;
            }

            _second += Time.deltaTime;

            if (_second < maxSecond)
            {
                return;
            }

            _second = 0;
            _roundTimer--;
            txtRoundTimer.text = $"{_roundTimer}";

            if (!_roundTimer.Equals(0))
            {
                return;
            }

            _roundIndex++;

            if (_roundIndex > rounds.Count - 1)
            {
                _isStartTimer = false;
                GameManager.GameManager.Instance.OnDraw();
                return;
            }

            GameManager.GameManager.Instance.SetGameState(General.GameState.Pause);
            GameManager.GameManager.Instance.OnBooster(rounds[_roundIndex]);
            DoBlinkTimer();
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            GameManager.GameManager.Instance.SetGameState(General.GameState.Pause);

            if (pauseStatus)
            {
                _timeExitApp = DateTime.Now;
            }
            else
            {
                if (!UIHelper.NetworkEvent(Lost))
                {
                    var time = _timeExitApp.Minute * 60 + _timeExitApp.Second + _roundTimer;

                    if (!(time <= DateTime.Now.Minute * 60 + DateTime.Now.Second))
                    {
                        Lost();
                    }
                }

                void Lost()
                {
                    UIManager.Instance.EnableDialog("You lost", dialog =>
                    {
                        dialog.SetActive(false);
                        GameManager.GameManager.Instance.OnLose();
                    });
                }
            }
        }
    }
}
using System;

namespace GameAssets.Scripts.GameInterface
{
    public interface IPieceBooster
    {
        void DoBooster(Action onFailed);
    }
}
using GameAssets.Scripts.GameBase;

namespace GameAssets.Scripts.GameInterface
{
    public interface IStatus
    {
        void BeAttacked(float dmg);
        void Attack(BaseCharacter bc);
        void Death();
    }
}
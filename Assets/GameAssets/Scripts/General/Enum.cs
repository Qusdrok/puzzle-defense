﻿namespace GameAssets.Scripts.General
{
    public enum VibrationType
    {
        Selection,
        Success,
        Warning,
        Failure,
        LightImpact,
        MediumImpact,
        HeavyImpact
    }

    public enum CharacterState
    {
        IdleOrRespawn = 0,
        Move = 1,
        Attack = 2,
        Death = 3
    }

    public enum GameState
    {
        Lobby,
        Loading,
        Pause,
        PreparePlay,
        Playing,
        Lose,
        Win,
        Draw
    }

    public enum UIType
    {
        None,
        UILobby,
        UILoading,
        UIPlay,
        UIGameStart,
        UIGameComplete,
        UIGameOver,
        UIGameDraw,
        UIDialog
    }

    public enum AnimationType
    {
        Fade,
        FadeScale,
        Zoom,
        Blink
    }

    public enum FitType
    {
        Uniform,
        Width,
        Height,
        FixedRows,
        FixedColumns,
        FixedBoth
    }

    public enum Alignment
    {
        Horizontal,
        Vertical
    }

    public enum UnitType
    {
        Castle,
        Melee,
        Fly,
        Range,
        Magician
    }

    public enum PriorityLevel
    {
        Low,
        Normal,
        High
    }

    public enum BoosterAffect
    {
        All,
        Soldier,
        Monster
    }

    public enum BoosterType
    {
        None,
        MatchSameAll,
        Match4Direction,
        Match9Direction
    }

    public enum BoosterAction
    {
        Touch,
        Swap,
        Both
    }

    public enum PieceType
    {
        Empty,
        Normal,
        Booster
    }
}
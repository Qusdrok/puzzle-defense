using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GameModel
{
    [CreateAssetMenu(fileName = "Round", menuName = "UI/Round", order = 0)]
    public class Round : ScriptableObject
    {
        public BoosterAffect boosterAffect;
        
        public string title;
        public int timer;

        [Header("Booster (%)")]
        public float hp;
        public float atk;
        public float atkSpeed;
        public float moveSpeed;
    }
}
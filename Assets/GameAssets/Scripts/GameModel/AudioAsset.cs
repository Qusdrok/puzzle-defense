﻿using GameAssets.Scripts.GameManager;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GameModel
{
    [CreateAssetMenu(fileName = "Audio Asset", menuName = "UI/Audio Asset", order = 0)]
    public class AudioAsset : ScriptableObject
    {
        [Header("Sound")] public Sprite uiSoundOn;
        public Sprite uiSoundOff;

        [Header("Music")] public Sprite uiMusicOn;
        public Sprite uiMusicOff;

        [Header("Vibration")] public Sprite uiVibrationOn;
        public Sprite uiVibrationOff;

        public void SetSpriteMusic(Image imgMusic, bool init = false)
        {
            if (!init)
            {
                UserDataManager.Instance.SetMusic(!UserDataManager.Instance.userDataSave.music);
            }

            imgMusic.sprite = UserDataManager.Instance.userDataSave.music ? uiMusicOn : uiMusicOff;
        }

        public void SetSpriteSound(Image imgSound, bool init = false)
        {
            if (!init)
            {
                UserDataManager.Instance.SetSound(!UserDataManager.Instance.userDataSave.sound);
            }

            imgSound.sprite = UserDataManager.Instance.userDataSave.sound ? uiSoundOn : uiSoundOff;
        }

        public void SetSpriteHaptic(Image imgHaptic, bool init = false)
        {
            if (!init)
            {
                UserDataManager.Instance.SetVibration(!UserDataManager.Instance.userDataSave.vibration);
            }

            imgHaptic.sprite = UserDataManager.Instance.userDataSave.vibration ? uiVibrationOn : uiVibrationOff;
        }
    }
}